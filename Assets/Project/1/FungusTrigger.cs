﻿using UnityEngine;
using System.Collections;
using Fungus;

public class FungusTrigger : MonoBehaviour {
		public string messageToSend = "";

		void OnTriggerEnter ( Collider other )
		{
				Fungus.Flowchart.BroadcastFungusMessage( messageToSend );
		}
}
