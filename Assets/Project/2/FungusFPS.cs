﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class FungusFPS : MonoBehaviour {
	
	public static FungusFPS Instance;
	
	public enum FungusFPSMode
	{
		FPS,
		Fungus
	}
	
	public GameObject player;
	
	public FirstPersonController fpsController;
		
	public Component[] scriptsToDisable;
	
	// Use this for initialization
	void Start () {
		if (Instance == null)
		{
			Instance = this;
		}
		if (player == null)
		{
			player = GameObject.FindGameObjectWithTag("Player");
		}
		if (fpsController == null)
		{
			fpsController = player.GetComponent<FirstPersonController>();
		}
		
		
	}
	
	public void OnFPSMode()
	{
		FPSMode();
	}
	public static void FPSMode()
	{
		Instance.fpsController.enabled = true;
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}
	
	public void OnFungusMode()
	{
		FungusMode();
	}
	public static void FungusMode()
	{
		Instance.fpsController.enabled = false;
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}
	
	public void FungusMessage(string fungusMessage)
	{
		OnFungusMode();
		Fungus.Flowchart.BroadcastFungusMessage(fungusMessage);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
